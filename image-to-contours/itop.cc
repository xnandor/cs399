////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Threshold Algorithm
//
// Algorithm Steps
//     1. Compute Luminance of Image
//     2. Generate Histogram of Luminance
//     3. Convolve Histogram (To decimate local mins)
//     4. Find 2 highest local maximums (compute 1st derivative and find roots)
//     5. Compute midpoint of peaks.
//     6. The x of midpoint is the threshold.
//     7. Perform threshold on luminance image with threshold value.
//
// Copyright Notice
//
//     by Eric Bischoff (alaneric1618@gmail.com)
//     Copyright 2014 Eric Bischoff (GNU GPLv3)
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// TODO:
//     - Add options for commanline interface with at least a debug flag.
//     - Improve readability.
//     - Imporve comments.
//     - Better decision making for choosing proper min.
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <cmath>
#include <iostream>
#include <cstring>
#include <fstream>

using namespace cv;
using namespace std;

int DEBUG = 0;


///////////////////////////////////////////////////////
//USAGE
static void Usage() {

}

///////////////////////////////////////////////////////
//DRAWHIST
//   Uses the window system in OpenCV to draw an histogram
//
void drawHist(int* hist, const char* string) {
  int height = 1100;
  Mat histImg = Mat::zeros(height, 255, CV_8UC1);
  for (int x=0; x<256; x++) {
    int h = hist[x]/10+550;
    for (int y=height-1; y>height-1-h; y--) {
      if (y <= height && y > 0) histImg.at<uchar>(y,x) = 0xff;
    }
  }
  for (int x = 0; x < 256; x++) {
    histImg.at<uchar>(height-550,x) = 0x80;
  }
  imshow(string, histImg);
}

///////////////////////////////////////////////////////
//READMAT
//   Reads a file from the OS and returns an OpenCV Mat.
//
//   char* filename - the c string name of the file.
//
Mat readMat(char* filename) {
  Mat img = imread(filename);
  if (!img.data) {
    cout << "Could not open or find image: " << filename << endl;
    exit(1);
  }
  return img;
}

////////////////////////////////////////////////////////////////////////////////
//CHANGESUFFIX
//    Written by Dr. Johnstone
//
void ChangeSuffix (string &s, string newSuffix) {
  string::size_type pos = s.find_last_of(".");
  if (pos != string::npos) {                   // 2nd argument contains a .
     s.erase (pos+1);                                 // erase suffix
     s += newSuffix;
  }
}

///////////////////////////////////////////////////////
//WRITEPOINTSFROMCONTOUR
//   Code taken from Dr. John Johnstone to output in his
//   point format.
//
void writePointsFromContour(vector<vector<Point > > contours,
                            vector<Vec4i> hierarchy,
                            char* filename) {
  int min_size = 5;
  int nCtr = 0;
  int allCtrs = 1;
  for (size_t k = 0; k < contours.size(); k++)
    if (contours[k].size() >= min_size &&
        (allCtrs || hierarchy[k][3] >= 0))  // hierarchy elt3 en
      nCtr++;
  string ptFileName(filename);
  string ptSuffix = "pt";
  ChangeSuffix(ptFileName, ptSuffix);
  ofstream outfile(ptFileName.c_str());
  outfile << "PT 2 " << nCtr << endl;
  outfile << "# " << filename << endl;
  for (size_t k = 0; k < contours.size(); k++) {
    if (contours[k].size() >= min_size && (allCtrs || hierarchy[k][3] >= 0)) {
      outfile << 1 << " " << contours[k].size() << endl;
      for (vector<Point>::iterator it = contours[k].begin();
           it != contours[k].end(); ++it)
        // image coordinates -> world coordinates: (x,y) -> (x
        outfile << ' ' << it->x << " " << -1*it->y << endl;
    }
  }
}

///////////////////////////////////////////////////////
//MAKELUMINANCE
//    Computes the luminance of an RGB image and flattens
//    it to one channel.
//
//    Mat img - is the RGB image
//
Mat makeLuminance(Mat img) {
  Mat lumImg = Mat::zeros(img.size(), CV_8UC1);
  for(int x = 0; x < img.cols; x++) {
    for(int y = 0; y < img.rows; y++) {
      uchar b = img.at<Vec3b>(y,x)[0];
      uchar g = img.at<Vec3b>(y,x)[1];
      uchar r = img.at<Vec3b>(y,x)[2];
      lumImg.at<uchar>(y,x) = 0.2126*r + 0.7152*g + 0.0722*b;
    }
  }
  return lumImg;
}

///////////////////////////////////////////////////////
//GETAUTOTHRESHOLD
//    Takes a single channel 8 bit image and generates an
//    optimal value to threshold the image.
//
//    Mat img - takes a 1-channel 8-bit image
//
int getAutoThreshold(Mat img) {
  //Compute Luminance
  Mat lumImg = makeLuminance(img);
  //Histogram
  int* hist = new int[256];
  //clear histogram
  for (int i = 0; i < 256; i++) {
    hist[i] = 0;
  }
  //populate histogram
  for(int x = 0; x < lumImg.cols; x++) {
    for(int y = 0; y < lumImg.rows; y++) {
      hist[(unsigned int)lumImg.at<uchar>(y,x)] += 1;
    }
  }
  //Box Convolution
  int boxSize = 10;
  double weight = (1.0/(double)boxSize);
  int* conHist = new int[256];
  //clear conHist bits
  for (int i = 0; i < 256; i++) {
    conHist[i] = 0;
  }
  for (int t = 0; t < 256-boxSize; t++) {
    double sum = 0;
    for (int i = 0; i < boxSize; i++) {
      sum += (hist[t+i]*weight);
    }
    conHist[t] = sum;
  }
  //Find Local Maximums (from convoluted histogram).
  int* dHist = new int[256]; //first dirivative
  for (int i = 0; i < 256; i++) {
    dHist[i] = ((int)conHist[i+1] - (int)conHist[i]);
  }
  int* maxs = new int[256];
  for (int i = 0; i < 256; i++) {
    int one = dHist[i];
    int two = dHist[i+1];
    if (one >= 0 && two < 0) {
      maxs[i] = 10000;
    } else {
      maxs[i] = -10000;
    }
  }
  //Find two heighest peaks
  int indexOfMax = 0;
  int max = 0;
  int indexOfSecondMax = 0;
  int secondMax = 0;
  for (int i = 0; i < 256; i++) { //first peak
    if (maxs[i] > 0) {
      if (conHist[i] > max) {
        max = conHist[i];
        indexOfMax = i;
      }
    }
  }
  for (int i = 0; i < 256; i++) { //second peak
    if (maxs[i] > 0) {
      if ((conHist[i] > secondMax) && (abs(i-indexOfMax) >= 10)) {
        secondMax = conHist[i];
        indexOfSecondMax = i;
      }
    }
  }
  //Delete Data
  delete hist;
  delete conHist;
  lumImg.release();
  //Calculate midpoint between peaks.
  int thresholdValue = (indexOfMax + indexOfSecondMax) / 2;
      //Perform final threshold
  return thresholdValue;
}



///////////////////////////////////////////////////////
//GETCONTOURS
//    This returns a list of list of points which
//    represents the contours of an image.
//
//    Mat binaryMat - should be a B&W image.
//
vector<vector<Point> > getContours(Mat binaryMat) {
  vector<vector<Point> > contours;
  Mat copy = Mat::zeros(binaryMat.size(), CV_8UC1);
  for(int x = 0; x < copy.cols; x++) { 
    for(int y = 0; y < copy.rows; y++) {
      uchar value = binaryMat.at<uchar>(y,x);
      copy.at<uchar>(y,x) = value;
    }
  }
  //findContours(copy, contours, CV_RETR_LIST, CV_CHAIN_APPROX_TC89_L1, Point(0,0));
  findContours(copy, contours, CV_RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS, Point(0,0));
  copy.release();
  return contours;
}

///////////////////////////////////////////////////////
//GETCONTOURSHIERARCHY
//    This should be used in conjunction with getContours.
//    It returns an hierarchy used by OpenCV.
//
vector<Vec4i> getContoursHierarchy(Mat binaryMat) {
  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;
  Mat copy = Mat::zeros(binaryMat.size(), CV_8UC1);
  for(int x = 0; x < copy.cols; x++) { 
    for(int y = 0; y < copy.rows; y++) {
      uchar value = binaryMat.at<uchar>(y,x);
      copy.at<uchar>(y,x) = value;
    }
  }
  findContours(copy, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS, Point(0,0));
  copy.release();
  return hierarchy;
}

///////////////////////////////////////////////////////
//MAIN
int main (int argc, char **argv) {
  Mat img = readMat(argv[1]);
  int thresholdValue = getAutoThreshold(img);
  Mat imgLum = makeLuminance(img);
  Mat imgThresh = Mat::zeros(imgLum.size(), CV_8UC1);
  threshold(imgLum, imgThresh, thresholdValue, 255, THRESH_BINARY_INV);
  vector<vector<Point> > contours = getContours(imgThresh);
  vector<Vec4i> hierarchy = getContoursHierarchy(imgThresh);
  if (argc > 2) writePointsFromContour(contours, hierarchy, argv[2]);
}
