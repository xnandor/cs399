#!/bin/bash
#
# This script automates the using a program with test data. 
# Eric A. Bischoff Copyright 2014
#

function usage() {
    echo "USAGE - "
    echo "     testdata.sh [-p <program>] <-s <source-directory>> <-t <target-directory>>"
    echo ""
    exit 1
}

function show_help() {
    echo "TODO: write help."
}

##INIT
PROGRAM=""
SOURCE_DIR=""
TARGET_DIR=""
ALL=""

##GET OPTIONS
OPTIND=1
while getopts "p:s:t:" opt; do
    case "$opt" in
        p)
            PROGRAM=$OPTARG
            ;;
        h|\?)
            show_help
            exit 0
            ;;
        s)
            SOURCE_DIR=$OPTARG
            ;;
        t)
            TARGET_DIR=$OPTARG
            ;;
    esac
done
shift $((OPTIND-1))
[ "$1" = "--" ] && shift

##FIND PROGRAM IN CURRENT DIRECTORY (if needed)
if [ -z "$PROGRAM" ]; then
    for file in `ls -p | grep -v '/$'`; do
        if [[ `otool -h $file` == *Mach*header* ]]; then
            PROGRAM="$file"
        fi
    done
fi

##Validate settings
if [ -z "$PROGRAM" ]; then
    usage
    exit 1
fi
if [ -z "$SOURCE_DIR" ]; then
    usage
    exit 1
fi

##RUN ALL TEST TOGETHER
if [ -n "$ALL" ]; then
    ##RUN TESTS
    for file in `ls $SOURCE_DIR`; do
        #$PROGRAM $SOURCE_DIR$file $file &      ...BAK
        $PROGRAM $SOURCE_DIR$file $TARGET_DIR$file &
    done

    ##STD OUT
    echo 'PRESS ANY KEY TO CLOSE ALL'
    read -n 1 X
    #    exec >> /dev/null
    #    exec 2>&1

    ##CLOSE TESTS
    for file in `ls $SOURCE_DIR`; do
        NAME="$PROGRAM $SOURCE_DIR$file"
        PSID=`ps | grep "$NAME" | grep -v "grep" | cut -d' ' -f1`
        kill -9 $PSID
    done
    #Remove next line to revert testdata.sh
    mv *.t.* $TARGET_DIR/
    exit 0
fi

##RUN EACH TEST ALONE
echo "Press any key to goto next item:"
#exec >> /dev/null
#exec 2>&1
for file in `ls $SOURCE_DIR`; do
    #$PROGRAM $SOURCE_DIR$file $file &      ...BAK
    $PROGRAM $SOURCE_DIR$file $TARGET_DIR$file &
    read -n 1 X
    NAME="$PROGRAM $SOURCE_DIR$file"
    PSID=`ps | grep "$NAME" | grep -v "grep" | cut -d' ' -f1`
    kill -9 $PSID
done
exit 0

