\documentclass[prodmode, acmcustom]{acmsmall}

\usepackage{graphicx}
\usepackage{setspace}
\doublespace

% Metadata Information
\acmVolume{1}
\acmNumber{1}
\acmArticle{1}
\acmYear{2014}
\acmMonth{12}
\def\@journelName{TEST}

\title{Handwritten Signatures: From Images to Pen Strokes}
\author{Eric A. Bischoff \affil{University of Alabama at Birmingham}}

\begin{document}
%\markboth{Eric A. Bischoff}{Handwritten Signatures: From Image Space to Parametric Curves}

\begin{abstract}

\begin{center}
  \includegraphics[width=400px]{media/beforeafter}
  {\bf \caption{Image (on left) and Point Cloud (on right)}}
\end{center}

{\bf ABSTRACT}

Sometimes in the field of bioinformatics it is useful to perform mathematical analysis on handwritten signatures.
In this paper we propose an algorithm for converting a digital image of a handwritten signature into an ordered point cloud.
In particular we obtain the straight skeleton of a signature and use it to resolve the path a pen might have traveled to produce the image.
After preprocessing an image-space representation we arrive at the outer contours which represent the pen stroke boundaries.
These boundaries can be used to generate a straight skeleton that contains all the points needed to approximate the pen strokes.
We then provide a technique for reconstructing areas in the straight skeleton and inferring pen directionallity.

\end{abstract}

%\category{I.4.0}{IMAGE PROCESSING AND COMPUTER VISION}{General}
%\terms{Image processing software}
%\keywords{signature, analysis, image, curves, handwriting, handwritten}
\maketitle


\section{Segmenting the Signature}

As imaging technology continues grow in quality and shrink in price, more handwritten signatures are being stored as images.
Images of signatures are important for financial records, contract negotiations and various other applications.
The volume of signatures stored in databases makes the processing and automation of signatures necessary for verifying and recalling them in larger systems.
We start our algorithm by emulating the human brain and segmenting the image into its visible regions \cite{segmentation}.
Since it is convention for images to be written in contrasting lightness, as opposed to contrasting color or texture, we proceed under this assumption.


\subsection{Preprocessing}

For our sample data we selected fifty handwritten signatures from an online service called Wikimedia.
Each one was taken from a famous scientist or musician.
Here is an example image:
\begin{center}
  \includegraphics[height=100px]{media/locke}
\end{center}

It is common with many work-flows for images to vary slightly in their quality.
Sometimes the background will be represented by white instead of an alpha.
To handle these types of problems we preprocess the images in batch, removing any alpha channel and replacing it with white.

\subsection{Autothreshold}

The image must ultimately be represented as a bit map.
That is, each pixel must be fully "on" or "off".
Each image may or may not have anti-aliasing.
There may also be variety of intensities in the texture of a pen.
We created an autothreshold algorithm to support these cases.
Our overall goal is to generate a histogram which has two primary peaks.
The midpoint of the two peaks will be where we divide the image.
Here are the steps for the algorithm:

{\bf Compute Luminance of Target Image} \\

$$
lum = 0.2126*red + 0.7152*green + 0.0722*blue,
$$
where $lum$ is the destination greyscale value and $red$ $green$ $blue$ are target channel values \cite{lum}.

{\bf Generate Luminance Histogram} \\
\begin{center}
  \includegraphics[height=100px]{media/hist}

  {\bf \caption{Luminance Histogram}}
\end{center}

{\bf Convolve Luminance Histogram} \\
This is to decimate local features in the shape of the histogram.
Smaller peaks will disappear making larger peaks stand out more.
We found that a box convolution worked just fine \cite{box}.
Future implementations may use a gaussian convolution instead of a box convolution \cite{gauss2}.
For the original Japaneese and English paper see the ``Theory of Patter Recognition'' \cite{gauss1}.
\begin{center}
  \includegraphics[height=100px]{media/conv}

  {\bf \caption{After Box Convolution}}
\end{center}

{\bf Compute First Derivative of Smooth Luminance Histogram} \\
The first derivative is used for finding local minimums and maximums.
We care about the maximums since they will help us identify peaks.
The max is where the derivative crosses the x-axis from positive to negative.
\begin{center}
  \includegraphics[height=100px]{media/deri}

  {\bf \caption{1st Derivative}}
\end{center}

{\bf Find Threshold from Maximums} \\
Find the midpoint between the two highest peaks using the following:
$$
midpoint = \frac{\big( index_{max1} + index_{max2} \big)}{2}
$$
For an 8-bit image the threshold value will be in between 0 and 255.
This means that we split the image into black and white on this value.
Everything lower than the threshold value will be black and everything higher will be white.

\subsection{Autothreshold Results}

Of the fifty images processed, forty-three of them had better results with auto threshold than if the default threshold value of two-hundred was used.
Six of the images saw no difference between an automatically determined threshold and one with a default value.
Only one image had better results without an automatically determined threshold value.
Here is a summary of the results: \\
\begin{center}
{\footnotesize{\bf Average Threshold Found:} $137 \pm (\approx) 47$ \\
{\bf Maximum Threshold Found:} $239$ \\
{\bf Minimum Threshold Found:} $15$ \\
}
\end{center}

\section{Skeleton}

To interpret the path the pen took as someone writes their signature, we are looking for a kind of midpoint between the two outer walls of the ink.
While a midpoint is a point on a one dimensional object, we are looking for a midpoint on a two dimensional object.
If the pen is thought of as having a radius we can constrain that two dimensional object to be a circle.
Though the radius of the circle changes it remains present in some form throughout every pen stroke.
This idea is firmly connected to an objects medial axis \cite{axis}.
As Blum points out, the medial axis as a tool is more related to biological problems than classical science and measuring.
Since a human signature is biological in nature the medial axis a great representation for a human pen stroke.
In order to simplify things we approximate the medial axis with a straight skeleton \cite{skeletons}.
To get to the skeleton, we first need our segmented regions to be represented as closed polygons with holes \cite{polyholes}.
We call these our contours.

\subsection{Contours}

Black pixels represent the background and white pixels represent the ink.
The transition from black to white represents the edge of the pen stroke.
By following the white pixels around the edges we can arrive at contours \cite{contours}.
Collectively, the edges come together to form contours like the one shown in the figure.
\begin{center}
  \includegraphics[height=130px]{media/contours}

  {\bf \caption{Contours}}
\end{center}
The contour is built of numerous ordered points:
\begin{center}
  \includegraphics[height=130px]{media/contourpoints}

  {\bf \caption{(Many Points Close Together)}}
\end{center}
Since a contour's points are defined in a discrete image space, they are all discrete.
Furthermore, there is one contour point for every boundary pixel.
This results in more boundary points than are helpful for later determining the skeleton.
We decimate as many points as possible without losing the general shape:
\begin{center}
  \includegraphics[height=130px]{media/contourpointsreduced}

  {\bf \caption{Contours After Decimation}}
\end{center}
We achieve this by using the Douglas-Peucker algorithm \cite{survey}.


\subsection{Obtaining Points to Represent Curvature}

We use Felkel's straight skeleton algorithm taken from CGAL \cite{skeletonalg}:
\begin{center}
  \includegraphics[height=130px]{media/skeleton}

  {\bf \caption{Straight Skeleton Without Bondary Vertices and Edge}}
\end{center}
This is descrete analogy of a medial axis \shortcite{axis}.
This greatly simplifies the pruning process as well as other processes \cite{medialpruning}.
Also, since our original data is coming from image space we have less motivation for continuous representations.

\subsection{Pruning}

Although pruning a straight skeleton would typically be a complex task, it's trivial for our application.
Since we are already using the Douglas-Peucker algorithm our contour vertices are guaranteed to produce a shallow Skeleton \shortcite{survey}.
Moreover, there will be few skeletal edges between boundary vertices and the innermost vertices.
We can sufficiently prune the skeleton by removing any edges that contain a boundary vertex.
After pruning we are left with:
\begin{center}
  \includegraphics[height=130px]{media/prunedskeleton}

  {\bf \caption{Pruned Skeleton}}
\end{center}

\section{Pen Strokes}

Emulating the stroke of a human hand requires that all the points on our skeleton are ordered.
Here are two questions that are important to answer:

1. {\it Where did the pen first touch down on a given character?}

2. {\it As the pen crosses an intersection, such as a junction on the letter 'x', which direction should the pen come out of the intersection?}

\subsection{Starting Points}

After getting a pruned skeleton, a list of potential starting points can be generated by a simple rule.
If the degree of a vertex is one than it is a potential end point.
In the example of the 'n' in "John Lock" there are three potential endpoints colored in green:
\begin{center}
  \includegraphics[height=130px]{media/endpoints}

  {\bf \caption{Starting Points in Green}}
\end{center}

There should be some analysis of the pen direction and the current character in order to determine the starting point.
Currently we select the upper-left most point.

\subsection{Intersections}

There are two types of skeletal intersections.
For both cases we are going to remove any vertex whose degree is greater than two.
We will also remove their adjacent edges.
Our general strategy is to reconstruct the intersections in order to obtain directional information when we order our vertices.

{\bf Type I}\\
The first type is a degenerate case where the pen overlaps itself and creates a skeleton vertex of degree three or greater.
It can be seen in the 'h' of "John":
\begin{center}
  \includegraphics[height=130px]{media/threeway}

  {\bf \caption{Three-way Intersection In 'h'}}
\end{center}
To handle this case we must label and compare all the different combinations.
For the example above there are three vertices.
After labeling each vertex 'a', 'b' and 'c' we compare unordered combinations without replacement.
So our options are $a \to b$, $a \to c$ or $b \to c$.
Following the same line of thinking used for degree three vertices, we can generalize the number of combinations for a higher degree with:
$$
\left(
  \begin{array}{c}
    V_n \\
    2
  \end{array}
\right) = E_n
$$
...where $V_n$ is the number of neighboring vertices to the intersection vertex and $E_n$ is the number of possible new edges.

For each potential vertex that needs to be connected up we can image a macro vector.
The macro vector can be obtained by following the edges from the neighboring vertex to the intersection and determining a direction.
By comparing macro vectors we can determine which vertices should be connected.

{\bf b. Type II}\\
The second type is where the pen crosses itself and creates a skeleton with multiple degree three vertices.
This can be seen in the 'L' in "Locke":
\begin{center}
  \includegraphics[height=130px]{media/fourway}

  {\bf \caption{Four-way Intersection in 'L'}}
\end{center}
Semantically we understand that these two intersections should be treated as one intersection.
As long as intersections are within some $\epsilon$ they should be merged together.
Our method is to draw the largest circle possible containing the possible intersection points.
If there are more than two then the circle should contain the two points that are farthest apart.
Any vertices that intersect the circle should be permanently removed.
\begin{center}
  \includegraphics[height=130px]{media/lmerged}

  {\bf \caption{Merged Four-way Intersection}}
\end{center}
They will be reconstructed.
All the vertices that were cut off should be marked.
Then we perform the algorithm for Type II on the marked vertices.

\section{Conclusion and Future Work}

We have introduced a novel approach for extracting an ordered point cloud from a signature's image.
By approximating the medial axis with a straight skeleton we were able to obtain points.
Points can then be arranged into a particular order by handling special cases around intersections.
Our resulting points seem to capture the general form of the signature while adding an intuitive data representation.

For future work we wish to better detect where a human hand might have started writing.
We would also come up with a more robust way of reconstructing intersection points.
With these goals completed the points could be converted into a curve such as a B-Spline \cite{bspline}.
Their curvatures could be compared to determine authorship information.

\begin{center}
  \includegraphics[height=130px]{media/final}

  {\bf \caption{Final Point Cloud}}
\end{center}

\newpage
\singlespace
\bibliographystyle{ACM-Reference-Format-Journals}
%\bibliographystyle{plain}
\bibliography{signature}
\end{document}


