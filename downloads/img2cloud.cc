// img2cloud.cc
// image of a 'clean' object(s) -> pt cloud(s) defining the object's boundary
//    the image of an object is 'clean' if it can be segmented by thresholding
//    examples: leaf: ~/Data/img/leaf/redoakbottom.jpg, 
//              signature: ~/Data/img/signature/niels_bohr_signature.png
//    each point cloud, being ordered, effectively defines a polygon
// Copyright 2014 John K. Johnstone

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
using namespace cv;
                           // reference OpenCV code: 'samples/cpp/contours2.cpp'
                                                    // demhist.cpp for Mat::ones
// fns used: imread, threshold, findContours, approxPolyDP, drawContours, imshow

#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
using namespace std;

vector<vector<Point> > contours;
vector<Vec4i> hierarchy;

static void Usage()
{
  cout
  << "thresholdable image -> point cloud of boundary" << endl
  << "   -t #: threshold: grayscale>thresh -> maxval (default=200)" << endl
  << "   -e #: epsilon used in decimation (default=1)" << endl
  << "   -d: do not decimate (default=do decimate)" << endl
  << "   -a: all contours (default=only external contours)" << endl
  << "   -D: debug mode" << endl
  << "   -b: batch mode" << endl
  << "   -m #: minimum size of a contour, ow discarded (default=5)" << endl
  << "   -p: output as polygons" << endl
  << endl 
  << "Input:  foo.img (color/grayscale, jpg/png/ppm/tiff)" << endl
  << "Output: optional (-b option): foo.pt or foo.plgn (-p option) " << endl;
  exit (-1);
}

/*******************************************************************************
  Change the suffix of a string.

<->s:        the string to be changed
->newSuffix: the desired new suffix (the string after the first dot)
*******************************************************************************/

void ChangeSuffix (string &s, string newSuffix)
{
  string::size_type pos = s.find_last_of(".");
  if (pos != string::npos)	                    // 2nd argument contains a .
   {
    s.erase (pos+1);		                                 // erase suffix
    s += newSuffix;
   }
}

/*******************************************************************************
algorithm:
- read, as grayscale image
- threshold, to binary image
- find all contours, internal and external (a collection of point clouds)
- decimate the contours
- remove polygons smaller than the minimum size
- optionally output the contours in PT or PLGN format
- draw the contours
*******************************************************************************/

int main (int argc, char **argv)
{
  int thresh = 200; // src(x,y) > thresh => becomes white
  float eps = 1;
  bool batch=0;
  bool decimate=1;
  bool allCtrs=0; // extract all contours, not just external contours?
  bool debug=1;
  int min_size = 5;
  bool store_as_polygon = 0;

  if (argc == 1) Usage();
  int args_parsed=1;
  while (args_parsed < argc)
   {
    if ('-' == argv[args_parsed][0])
      switch (argv[args_parsed++][1])
      {
      case 't': thresh = atoi (argv[args_parsed++]); break;
      case 'e': eps = atof (argv[args_parsed++]); break;
      case 'd': decimate=0; break;
      case 'a': allCtrs=1; break;
      case 'b': batch=1; break;
      case 'm': min_size = atoi (argv[args_parsed++]); break;
      case 'p': store_as_polygon = 1; break;
      case 'h':
      default: Usage(); break;
      }
   else args_parsed++;
  }

  Mat img, imgThresh;
  vector<vector<Point> > contours0;

  img = imread (argv[argc-1], CV_LOAD_IMAGE_GRAYSCALE);
  if (!img.data) { cout<<"Could not open or find image"<<endl; return -1; }
  threshold (img, imgThresh, thresh, 255, THRESH_BINARY); // >thresh->white(255)
  findContours (imgThresh, contours0, hierarchy,
	       	CV_RETR_CCOMP,    // external and hole (holes not distinguished)
		CHAIN_APPROX_SIMPLE);                // compress horiz/vert/diag






  if (decimate) {
    contours.resize(contours0.size());
    bool closed = true;
    for (size_t k = 0; k < contours0.size(); k++) {
      approxPolyDP(Mat(contours0[k]), contours[k], eps, closed);
    }
  }






  else 
    contours = contours0;

  if (batch)
   {
    if (!store_as_polygon)
     {
      int nCtr=0;
      for (size_t k = 0; k < contours.size(); k++)
	if (contours[k].size() >= min_size && 
	    (allCtrs || hierarchy[k][3] >= 0))  // hierarchy elt3 encodes parent
	  nCtr++;
      string ptFileName (argv[argc-1]);
      string ptSuffix = "pt";  ChangeSuffix (ptFileName, ptSuffix);
      ofstream outfile (ptFileName.c_str());
      outfile << "PT 2 " << nCtr << endl;
      outfile << "# " << argv[argc-1] << endl;
      for (size_t k = 0; k < contours.size(); k++)
	if (contours[k].size() >= min_size && 
	    (allCtrs || hierarchy[k][3] >= 0))
	  {
	    outfile << 1 << " " << contours[k].size() << endl;
	    for (vector<Point>::iterator it = contours[k].begin() ; 
		 it != contours[k].end(); ++it)
	      // image coordinates -> world coordinates: (x,y) -> (x,-y)
	      outfile << ' ' << it->x << " " << -1*it->y << endl;
	  }
     }
    else
     {
      string polygonFileName (argv[argc-1]);
      string suffix = "pn";  ChangeSuffix (polygonFileName, suffix);
      ofstream outfile (polygonFileName.c_str());
      outfile << "PLGN 2 " << contours.size() << endl;
      outfile << "# " << argv[argc-1] << endl;
      for (size_t k = 0; k < contours.size(); k++)
	{
	  outfile << "POLYGON " << contours[k].size() << endl;
	  for (vector<Point>::iterator it = contours[k].begin() ; 
	       it != contours[k].end(); ++it)
	    // image coordinates -> world coordinates: (x,y) -> (x,-y)
	    outfile << ' ' << it->x << " " << -1*it->y << endl;
	}
      outfile << "HIERARCHY" << endl;
      for (size_t k = 0; k < contours.size(); k++)
	{
	  for (int m=0; m<4; m++)
	    outfile << hierarchy[k][m] << " ";
	  outfile << endl;
	}
     }
   }
  else
   {
     Mat dst=Mat::ones (imgThresh.rows, imgThresh.cols, CV_8U)*255;  
     for (size_t k = 0; k < contours.size(); k++)
       if (contours[k].size() >= min_size && (allCtrs || hierarchy[k][3] >= 0)) 
	 drawContours (dst, contours, k, Scalar(0,0,0), 2);
     // imwrite (argv[2], dst);
     namedWindow ("img -> cloud");
     imshow ("img -> cloud", dst);
     waitKey();
   }
  return 0;
}

