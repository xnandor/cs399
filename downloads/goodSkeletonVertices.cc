typedef Polyhedron::Vertex_iterator                          VertexIterator;
typedef CGAL::Polyhedron_3<Kernel>                           Polyhedron;


Polyhedron P;
for (VertexIterator vh = P.vertices_begin(); vh != P.vertices_end(); ++vh) {
    VEcirculator e = vh->vertex_begin(); // does this need to be constant?
    std::size_t n = circulator_size (e);
    if (n == 2) good else bad;
}

for all good v
  choose a good v to start
  oldv = v
  v = v->halfedge()->opposite()->vertex()
  while (v is good)
    use circulator from v to find 2 nbour vertex options newv1 and newv2
    if (newv1 != oldv) newv = newv1 else newv = newv2
    oldv = v
    v = newv
  this collects a chain of good vertices

now handle bad v's

  for each bad v
   - discover the chains that interact with this bad v (or its cluster of bad v)
     (A,B,C,D in our picture)
 - for each chain, extract least squares fitting line of its last 10 elements (as a vector)
     - that is, in practice, first point - 10th point
     - call it a macro-vector

     in the typical case, there are 4 macro-vectors (simple intersection)

     - pair A with correct B/C/D using obvious angle relationship

     in another typical case, there are 3 macro-vectors (the h and n in John)

