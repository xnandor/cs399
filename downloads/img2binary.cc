// img2binary.cc ; johnstone (jkj@uab.edu)
// image -> binary image using thresholding, so that object of interest is black

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <cmath>
#include <iostream>

using namespace cv;
using namespace std;

static void Usage()
{
  cout
  << "img -> binary image" << endl
  << "   -t #: threshold: grayscale>thresh -> maxval (default=200)" << endl
  << "img2binary foo.img [fooBinary.img]" << endl
  << "  valid img formats: jpg,png,ppm,tiff" << endl;
}

int main (int argc, char **argv)
{
  int thresh = 200; // maxval if src(x,y) > thresh (in our case, maxval = white)
  if (argc == 1) { Usage(); return -1; }
  int args_parsed=1;
  while (args_parsed < argc)
   {
    if ('-' == argv[args_parsed][0])
      switch (argv[args_parsed++][1])
      {
      case 't': thresh = atoi (argv[args_parsed++]); break;
      case 'h': 
      default:	Usage(); return -1; break;
      }
    else args_parsed++;
   }
  // ---------------------------------------------------------------------------
  Mat img; img = imread (argv[1], CV_LOAD_IMAGE_GRAYSCALE);
  if (!img.data) { cout<<"Could not open or find image"<<endl; return -1; }
  Mat imgThresh; 
  threshold (img, imgThresh, thresh, 255, THRESH_BINARY);    // >thresh -> white
  if (argc>2) imwrite (argv[2], imgThresh); // optional save of binary image
  namedWindow ("binary image");
  imshow ("binary image", imgThresh);
  waitKey();
  return 0;
}

// action item: choose the threshold through analysis of the image
