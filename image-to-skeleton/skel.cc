////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Image to Skeleton
//
// Copyright Notice
//
//     by Eric Bischoff (alaneric1618@gmail.com)
//     Copyright 2014 Eric Bischoff (GNU GPLv3)
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <boost/shared_ptr.hpp>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/create_straight_skeleton_2.h>
#include <CGAL/create_straight_skeleton_from_polygon_with_holes_2.h>

#include <cmath>
#include <cfloat>
#include <cstdint>
#include <float.h>
#include <iostream>
#include <fstream>


#define pi 3.14159

using namespace std;
using namespace cv;

typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
typedef K::Point_2                                           CGALPoint;
typedef CGAL::Polygon_2<K>                                   Polygon;
typedef CGAL::Polygon_with_holes_2<K>                        Polygon_with_holes;
typedef CGAL::Straight_skeleton_2<K>                         SS;
typedef boost::shared_ptr<SS>                                SSPtr;
typedef SS::Vertex                                           V;
typedef SS::Vertex_handle                                    VH;
typedef SS::Halfedge                                         Halfedge;
typedef SS::Vertex::Halfedge_around_vertex_circulator        HVC;

bool DEBUG = false;
Mat imgContours;


///////////////////////////////////////////////////////
//Draw Point
//
void drawPoint(Mat img, Point point, int r, int g, int b) {
  circle(img, point, 1, CV_RGB(r,g,b));
}

///////////////////////////////////////////////////////
//Draw Halfedge
//
void drawHalfedge(Mat img, SS::Halfedge e, int r, int g, int b) {
  CGALPoint point1 = e.vertex()->point();
  CGALPoint point2 = e.opposite()->vertex()->point();
  int x1 = point1.x();
  int y1 = point1.y();
  int x2 = point2.x();
  int y2 = point2.y();
  line(img, Point2i(y1, x1), Point2i(y2,x2), CV_RGB(r, g, b));
}

////////////////////////////////////////////////////////
//Get Direction Weight
//
double getDirectionWeight(Point p, Point c, Point n) {
  Point2d dir = Point( (c.x-p.x), (c.y-p.y) ); //Direction Vector
  Point2d nDir = Point( (n.x-c.x), (n.y-c.y) ); //Direction Vector of next
  double dirMag = sqrt(pow(dir.x,2)+pow(dir.y,2));
  double nDirMag = sqrt(pow(nDir.x,2)+pow(nDir.y,2));
  //Normalize
  dir = Point2d(dir.x/dirMag, dir.y/dirMag);
  nDir = Point2d(nDir.x/nDirMag, nDir.y/nDirMag);
  //Update mags
  dirMag = sqrt(pow(dir.x,2)+pow(dir.y,2));
  nDirMag = sqrt(pow(nDir.x,2)+pow(nDir.y,2));
  //Find distance between current point and next point.
  double dot = dir.x*nDir.x + dir.y*nDir.y;
  double angle = acos(dot/(dirMag*nDirMag));
  double weight = 2-cos(angle)/2.0;
  if (angle > 90) {
    weight = 10*weight;
  }
  // cout << "DOT: " << dot << endl;
  // cout << "DIR: " << dir.x << "," << dir.y << "    mag:"<< dirMag << endl;
  // cout << "nDIR: " << nDir.x << "," << nDir.y << "  mag:" << nDirMag << endl;
  // cout << "angle:" << angle*57.295779513 << "   weight:" << weight << endl;
  // cout << endl;
  return weight;
}

////////////////////////////////////////////////////////////////////////////////
//Get Distance
//
double getDistance(Point p1, Point p2) {
  return sqrt(pow(p2.x-p1.x,2) + pow(p2.y-p1.y,2));;
}

///////////////////////////////////////////////////////
//READMAT
//   Reads a file from the OS and returns an OpenCV Mat.
//
//   char* filename - the c string name of the file.
//
Mat readMat(char* filename) {
  Mat img = imread(filename);
  if (!img.data) {
    cout << "Could not open or find image: " << filename << endl;
    exit(1);
  }
  return img;
}

////////////////////////////////////////////////////////////////////////////////
//CHANGESUFFIX
//    Written by Dr. Johnstone
//
void ChangeSuffix (string &s, string newSuffix) {
  string::size_type pos = s.find_last_of(".");
  if (pos != string::npos) {                   // 2nd argument contains a .
     s.erase (pos+1);                                 // erase suffix
     s += newSuffix;
  }
}

///////////////////////////////////////////////////////
//WRITEPOINTSFROMCONTOUR
//   Code taken from Dr. John Johnstone to output in his
//   point format.
//
void writePointsFromContour(vector<vector<Point > > contours,
                            vector<Vec4i> hierarchy,
                            char* filename) {
  int min_size = 5;
  int nCtr = 0;
  int allCtrs = 1;
  for (size_t k = 0; k < contours.size(); k++)
    if (contours[k].size() >= min_size &&
        (allCtrs || hierarchy[k][3] >= 0))  // hierarchy elt3 en
      nCtr++;
  string ptFileName(filename);
  string ptSuffix = "pt";
  ChangeSuffix(ptFileName, ptSuffix);
  ofstream outfile(ptFileName.c_str());
  outfile << "PT 2 " << nCtr << endl;
  outfile << "# " << filename << endl;
  for (size_t k = 0; k < contours.size(); k++) {
    if (contours[k].size() >= min_size && (allCtrs || hierarchy[k][3] >= 0)) {
      outfile << 1 << " " << contours[k].size() << endl;
      for (vector<Point>::iterator it = contours[k].begin();
           it != contours[k].end(); ++it)
        // image coordinates -> world coordinates: (x,y) -> (x
        outfile << ' ' << it->x << " " << -1*it->y << endl;
    }
  }
}

///////////////////////////////////////////////////////
//MAKELUMINANCE
//    Computes the luminance of an RGB image and flattens
//    it to one channel.
//
//    Mat img - is the RGB image
//
Mat makeLuminance(Mat img) {
  Mat lumImg = Mat::zeros(img.size(), CV_8UC1);
  for(int x = 0; x < img.cols; x++) {
    for(int y = 0; y < img.rows; y++) {
      uchar b = img.at<Vec3b>(y,x)[0];
      uchar g = img.at<Vec3b>(y,x)[1];
      uchar r = img.at<Vec3b>(y,x)[2];
      lumImg.at<uchar>(y,x) = 0.2126*r + 0.7152*g + 0.0722*b;
    }
  }
  return lumImg;
}

///////////////////////////////////////////////////////
//GETAUTOTHRESHOLD
//    Takes a single channel 8 bit image and generates an
//    optimal value to threshold the image.
//
//    Mat img - takes a 1-channel 8-bit image
//
int getAutoThreshold(Mat img) {
  //Compute Luminance
  Mat lumImg = makeLuminance(img);
  //Histogram
  int* hist = new int[256];
  //clear histogram
  for (int i = 0; i < 256; i++) {
    hist[i] = 0;
  }
  //populate histogram
  for(int x = 0; x < lumImg.cols; x++) {
    for(int y = 0; y < lumImg.rows; y++) {
      hist[(unsigned int)lumImg.at<uchar>(y,x)] += 1;
    }
  }
  //Box Convolution
  int boxSize = 10;
  double weight = (1.0/(double)boxSize);
  int* conHist = new int[256];
  //clear conHist bits
  for (int i = 0; i < 256; i++) {
    conHist[i] = 0;
  }
  for (int t = 0; t < 256-boxSize; t++) {
    double sum = 0;
    for (int i = 0; i < boxSize; i++) {
      sum += (hist[t+i]*weight);
    }
    conHist[t] = sum;
  }
  //Find Local Maximums (from convoluted histogram).
  int* dHist = new int[256]; //first dirivative
  for (int i = 0; i < 256; i++) {
    dHist[i] = ((int)conHist[i+1] - (int)conHist[i]);
  }
  int* maxs = new int[256];
  for (int i = 0; i < 256; i++) {
    int one = dHist[i];
    int two = dHist[i+1];
    if (one >= 0 && two < 0) {
      maxs[i] = 10000;
    } else {
      maxs[i] = -10000;
    }
  }
  //Find two heighest peaks
  int indexOfMax = 0;
  int max = 0;
  int indexOfSecondMax = 0;
  int secondMax = 0;
  for (int i = 0; i < 256; i++) { //first peak
    if (maxs[i] > 0) {
      if (conHist[i] > max) {
        max = conHist[i];
        indexOfMax = i;
      }
    }
  }
  for (int i = 0; i < 256; i++) { //second peak
    if (maxs[i] > 0) {
      if ((conHist[i] > secondMax) && (abs(i-indexOfMax) >= 10)) {
        secondMax = conHist[i];
        indexOfSecondMax = i;
      }
    }
  }
  //Delete Data
  delete hist;
  delete conHist;
  lumImg.release();
  //Calculate midpoint between peaks.
  int thresholdValue = (indexOfMax + indexOfSecondMax) / 2;
      //Perform final threshold
  return thresholdValue;
}



///////////////////////////////////////////////////////
//GETCONTOURS
//    This returns a list of list of points which
//    represents the contours of an image.
//
//    Mat binaryMat - should be a B&W image.
//
vector<vector<Point> > getContours(Mat binaryMat) {
  vector<vector<Point> > contours;
  Mat copy = Mat::zeros(binaryMat.size(), CV_8UC1);
  for(int x = 0; x < copy.cols; x++) {
    for(int y = 0; y < copy.rows; y++) {
      uchar value = binaryMat.at<uchar>(y,x);
      copy.at<uchar>(y,x) = value;
    }
  }
  //findContours(copy, contours, CV_RETR_LIST, CV_CHAIN_APPROX_TC89_L1, Point(0,0));
  findContours(copy, contours, CV_RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS, Point(0,0));
  copy.release();
  return contours;
}

///////////////////////////////////////////////////////
//GETCONTOURSHIERARCHY
//    This should be used in conjunction with getContours.
//    It returns an hierarchy used by OpenCV.
//
vector<Vec4i> getContoursHierarchy(Mat binaryMat) {
  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;
  Mat copy = Mat::zeros(binaryMat.size(), CV_8UC1);
  for(int x = 0; x < copy.cols; x++) {
    for(int y = 0; y < copy.rows; y++) {
      uchar value = binaryMat.at<uchar>(y,x);
      copy.at<uchar>(y,x) = value;
    }
  }
  findContours(copy, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS, Point(0,0));
  copy.release();
  return hierarchy;
}

bool hasVertex(vector<V> *vertices, V v) {
  bool found = false;
  for (vector<V>::iterator it = vertices->begin(); it != vertices->end(); it++) {
    V tmp = *it;
    if (v.id() == tmp.id()) found = true;
  }
  return found;
}

void removeVertex(vector<V> *vertices, V v) {
  for (vector<V>::iterator it = vertices->begin(); it != vertices->end(); it++) {
    V tmp = *it;
    if (v.id() == tmp.id()) {
      vertices->erase(it);
      it--;
    }
  }
}


bool hasIntersection(map<int, vector<V> > *intersections, V v) {
  bool found = false;
  for (map<int, vector<V> >::iterator it1 = intersections->begin(); it1 != intersections->end(); it1++) {
    vector<V> points = (*it1).second;
    for (vector<V>::iterator it2 = points.begin(); it2 != points.end(); it2++) {
      V tmp = *it2;
      if (v.id() == tmp.id()) found = true;
    }
  }
  return found;
}

void addIntersection(map<int, vector<V> > *intersections, int id, V v) {
  vector<V> *points = &(intersections->at(id));
  points->push_back(v);
}

void removeIntersection(map<int, vector<V> > *intersections, int id, V v) {
  vector<V> *points = &(intersections->at(id));
  for (vector<V>::iterator it = points->begin(); it != points->end(); it++) {
    V tmp = *it;
    if (v.id() == tmp.id()) {
      points->erase(it);
      it--;
    }
  }
}


pair<int, vector<V> > getIntersections(map<int, vector<V> > *intersections, V v) {
  bool found = false;
  pair<int, vector<V> > p;
  for (map<int, vector<V> >::iterator it1 = intersections->begin(); it1 != intersections->end(); it1++) {
    vector<V> points = (*it1).second;
    for (vector<V>::iterator it2 = points.begin(); it2 != points.end(); it2++) {
      V tmp = *it2;
      if (v.id() == tmp.id()) {
        found = true;
        p = *it1;
      }
    }
  }
  return p;
}

pair<int, vector<V> > getIntersectionsById(map<int, vector<V> > *intersections, int id) {
  pair<int, vector<V> > p;
  p.first = id;
  p.second = intersections->at(id);
  return p;
}


///////////////////////////////////////////////////////
//APPROXCONTOURS
//
vector<vector<Point> > approxContours(vector<vector<Point> > contours) {
  vector<vector<Point> > newContours;
  newContours.resize(contours.size());
  for (size_t i = 0; i < contours.size(); i++) {
    approxPolyDP(Mat(contours[i]), newContours[i], 1, true);
  }
  return newContours;
}

///////////////////////////////////////////////////////
//GET DEGREE
//
int getDegree(V v) {
  HVC hvc = v.halfedge_around_vertex_begin();
  size_t size = CGAL::circulator_size(hvc);
  int degree = 0;
  for (int i = 0; i < size; i++) {
    SS::Halfedge h = **hvc;
    V nv = *h.opposite()->vertex(); //neighboring vertex
    if (nv.is_skeleton()) {
      degree++;
    }
    ++hvc;
  }
  return degree;
}

///////////////////////////////////////////////////////
//OrderPoints
//
vector< vector<Point> > ssToOrderedPoints(SSPtr ss) {
  vector< vector<Point> > strokes;
  //GET SKELETON place in Vertex Vector
  vector<V> vertices;
  vector<V> endPoints;
  vector<V> intersectionPoints;
  map<int, vector<V> > intersectionMap;
  for (SS::Vertex_iterator it = ss->vertices_begin(); it != ss->vertices_end(); it++) {
    V v = *it;
    if (v.is_skeleton()) {
      int degree = getDegree(v);
      if (degree == 1) endPoints.push_back(v);
      if (degree == 2) vertices.push_back(v);
      if (degree >= 3) intersectionPoints.push_back(v);
    }
  }
  //COLLAPSE NEAR VERTEXES
  vector<V> newIntersectionPoints;
  double epsilon = 17;    //TODO: Make value dynamic based on pen width
  int intersectionId = 0;
  while (!intersectionPoints.empty()) {
    vector<V> computedVertices;
    V v = intersectionPoints.back();
    intersectionPoints.pop_back();
    computedVertices.push_back(v);
    bool wasMerged = false;
    for (vector<V>::iterator it = intersectionPoints.begin(); it != intersectionPoints.end(); it++) {
      V v2 = *it;
      CGALPoint p = v.point();
      CGALPoint p2 = v2.point();
      double dist = sqrt( pow( p2.x()-p.x(),2) +  pow( p2.y()-p.y(),2)  );
      if (dist < epsilon) {
        //MERGE
        wasMerged = true;
        computedVertices.push_back(v2);
        CGALPoint newPoint = CGALPoint(  (p2.x()+p.x())/2,   (p2.y()+p.y())/2  );
        v = V(0, newPoint);
        intersectionPoints.erase(it);
        it--;
        //ERASE POINTS IN CIRCLE
        double radius = dist/2.0;
        for (vector<V>::iterator it3 = vertices.begin(); it3 != vertices.end(); it3++) {
          V v3 = *it3;
          CGALPoint p = v.point();
          CGALPoint p3 = v3.point();
          double distFromV = sqrt( pow( p3.x()-p.x(),2) +  pow( p3.y()-p.y(),2)  );
          if (distFromV < radius) {
            vertices.erase(it3);
            it3--;
          }
        }
        p = v.point();
        circle(imgContours, Point(p.y(),p.x()), radius, CV_RGB(255,255,255));
        //END ERASE POINTS IN CIRCLE
      }
    }
    //ADD INTERSECTION ID TO NEIGHBORS
    vector<V> neighborPoints;
    for (vector<V>::iterator it = computedVertices.begin(); it != computedVertices.end(); it++) {
      V v = *it;
      int degree = getDegree(v);
      HVC hvc = v.halfedge_around_vertex_begin();
      size_t size = CGAL::circulator_size(hvc);
      for (int i = 0; i < size; i++) {
        SS::Halfedge h = **hvc;
        V nv  = *h.vertex(); //neighboring vertex
        V rnv = *h.opposite()->vertex(); //neighboring vertex
        //Check if found in vertices
        for (vector<V>::iterator it = vertices.begin(); it != vertices.end(); it++) {
          V tmp = *it;
          int id = tmp.id();
          if (id ==  nv.id() && !hasVertex(&neighborPoints,  nv)) neighborPoints.push_back( nv);
          if (id == rnv.id() && !hasVertex(&neighborPoints, rnv)) neighborPoints.push_back(rnv);
        }
        ++hvc;
      }
    }
    //END INTERSECTION ID TO NEIGHBORS
    intersectionMap[intersectionId] = neighborPoints;
    v.reset_id__internal__(intersectionId++);
    newIntersectionPoints.push_back(v);
  }
  intersectionPoints = newIntersectionPoints;
  //DRAW VERTS
  for (vector<V>::iterator it = vertices.begin(); it != vertices.end(); it++) {
    V v = *it;
    Point p = Point2i(v.point().y(), v.point().x());
    drawPoint(imgContours, p, 255, 255, 255);
    //DRAW LINEAR SKELETON
    int degree = getDegree(v);
    HVC hvc = v.halfedge_around_vertex_begin();
    size_t size = CGAL::circulator_size(hvc);
    for (int i = 0; i < size; i++) {
      SS::Halfedge h = **hvc;
      V nv = *h.opposite()->vertex(); //neighboring vertex
      if (nv.is_skeleton()) {
        int otherDegree = getDegree(nv);
        if ((degree == 2 && otherDegree == 2)) {
          drawHalfedge(imgContours, h, 75,75,75);
        }
      }
      ++hvc;
    }
    //END DRAW LINEAR SKELETON
  }
  //DRAW ENDPOINTS
  for (vector<V>::iterator it = endPoints.begin(); it != endPoints.end(); it++) {
    V v = *it;
    Point p = Point2i(v.point().y(), v.point().x());
    drawPoint(imgContours, p, 0, 255, 0);
  }
  //DRAW INTERSECTION POINTS
  for (vector<V>::iterator it = intersectionPoints.begin(); it != intersectionPoints.end(); it++) {
    V v = *it;
    Point p = Point2i(v.point().y(), v.point().x());
    drawPoint(imgContours, p, 255, 0, 0);
  }
  //ORDER ORDER ORDER ORDER ORDER ORDER ORDER ORDER ORDER ORDER ORDER ORDER ORDER
  //Pick A Starting Point
  int startingId = 0;
  V startingV;
  double min = 999999999999.9;
  if (!endPoints.empty()) {
    for (vector<V>::iterator it = endPoints.begin(); it != endPoints.end(); it++) {
      V v = *it;
      double weight = v.point().x()*0.33 + v.point().y()*0.66;
      if (weight < min) {
        min = weight;
        startingId = v.id();
        startingV = v;
      }
    }
  } else {
    startingId = vertices.back().id();
    startingV = vertices.back();
  }
  Point p = Point2i(startingV.point().y(), startingV.point().x());
  drawPoint(imgContours, p, 255, 0, 255);
  //Build Strokes
  V v  = startingV; //Current V
  V pv = startingV;
  vector<Point> points;
  points.push_back(Point2i(v.point().y(), v.point().x()));
  bool found = true;
  vector<V> returnPoints;
  while (found) {
    //cout << "   VERTEX: " << v.id() << endl;
    found = false;
    if (hasIntersection(&intersectionMap, v)) { //INTERSECTION
      pair<int, vector<V> > intersections = getIntersections(&intersectionMap, v);
      int id = intersections.first;
      int way = intersections.second.size();
      vector<V> options = intersections.second;
      if (options.size() > 1) {
        //CHOOSE
        int index  = 0;
        V choice = options[index];
        double weight = 0;
        for (int i = 0; i < options.size(); i++) {
          if (options[i].id() != v.id()) {
            choice = options[i];
          } else {
            returnPoints.push_back(v);
          }
        }
        points.push_back(Point2i(choice.point().y(), choice.point().x()));
        removeIntersection(&intersectionMap, id, choice);
        removeVertex(&vertices, choice);
        pv = v;
        v = choice;
        found = true;
      } else {
        removeIntersection(&intersectionMap, id, v);
        removeVertex(&vertices, v);
      }
      //Set Return Point If needed
    } else {                                     //STRAIGHT
      HVC hvc = v.halfedge_around_vertex_begin();
      for (int i = 0; i < CGAL::circulator_size(hvc); i++) {
        SS::Halfedge h = **hvc;
        V nv = *h.opposite()->vertex(); //neighboring vertex
        if (hasVertex(&vertices, nv) && !found) { //FOUND NEXT POINT From verts
          points.push_back(Point2i(nv.point().y(), nv.point().x()));
          removeVertex(&vertices, nv);
          pv = v;
          v = nv;
          found = true;
        }
        /////LOCATION WHERE INTERSECTIONOCCURED
        ++hvc;
      }

    }
    if (!found) { //If not found yet pick a new vertex from endpoints
      //Wrap up current Stroke.
      strokes.push_back(points);
      vector<Point> newPoints;
      //cout << "                   ";
      points = newPoints;
      if (!returnPoints.empty()) { //FIND NEXT POINT FROM RETURN LIST
        pv = v;
        v = returnPoints.back();
        returnPoints.pop_back();
        points.push_back(Point2i(v.point().y(), v.point().x()));
        removeVertex(&vertices, v);
        found = true;
      } else if (!endPoints.empty()) {  //FIND NEXT POINT FROM ENDS
        pv = v;
        v = endPoints.back();
        endPoints.pop_back();
        points.push_back(Point2i(v.point().y(), v.point().x()));
        removeVertex(&vertices, v);
        found = true;
      }
    }
  }
  return strokes;
}


///////////////////////////////////////////////////////
//MAIN
//
int main(int argc, char** argv) {
  vector< vector< vector<Point> > > allStrokes;
  Mat img = readMat(argv[1]);
  int thresholdValue = getAutoThreshold(img);
  Mat imgLum = makeLuminance(img);
  Mat imgThresh = Mat::zeros(imgLum.size(), CV_8UC1);
  imgContours = Mat::zeros(imgThresh.size(), CV_8UC3);
  threshold(imgLum, imgThresh, thresholdValue, 255, THRESH_BINARY_INV);
  vector<vector<Point> > contours = getContours(imgThresh);
  contours = approxContours(contours);
  vector<Vec4i> hierarchy = getContoursHierarchy(imgThresh);
  cout << "TOTAL CONTOURS: " << contours.size() << endl;
  //ITERATE THROUGH CONTOURS
  for(std::vector<int>::size_type i = 0; i != contours.size(); i++) {
    int IDX = i;
    int nextIDX = hierarchy[i][0];
    int previousIDX = hierarchy[i][1];
    int firstChildIDX = hierarchy[i][2];
    int parentIDX = hierarchy[i][3];
    cout << "  CONTOUR NUMBER: " << i << endl;
    //ONLY LOOK AT FIRST LEVEL CONTOURS
    if (parentIDX == -1) {
      //add external boundary
      Polygon poly;
      for (std::vector<int>::size_type j = 0; j != contours[i].size(); j++) {
        Point point = contours[i][j];
        int x = point.x;
        int y = point.y;
        poly.push_back( CGALPoint(y, x));
        //imgContours.at<Vec3b>(y,x)[1] = 255;
      }
      vector<Polygon> holes;
      //FIND HOLES
      for(std::vector<int>::size_type h = 0; h != contours.size(); h++) {
            int IDX = h;
            int nextIDX = hierarchy[h][0];
            int previousIDX = hierarchy[h][1];
            int firstChildIDX = hierarchy[h][2];
            int parentIDX = hierarchy[h][3];
            if (parentIDX == i) { //found hole
              Polygon hole; //get hole
              for (std::vector<int>::size_type j = 0; j != contours[h].size(); j++) {
                Point point = contours[h][j];
                int x = point.x;
                int y = point.y;
                hole.push_back( CGALPoint(point.y, point.x));
                //imgContours.at<Vec3b>(y,x)[1] = 255;
              }
              holes.push_back(hole);
            }
      }
      SSPtr iss;
      if (holes.size() <= 0) { //NO HOLES
        // You can pass the polygon via an iterator pair
        iss = CGAL::create_interior_straight_skeleton_2(poly.vertices_begin(), poly.vertices_end());
      } else { //HAS HOLES
        Polygon_with_holes complex_poly(poly);
        for (std::vector<int>::size_type h = 0; h != holes.size(); h++) {
          complex_poly.add_hole(holes.at(h));
        }
        iss = CGAL::create_interior_straight_skeleton_2(complex_poly);
      }

////////////////////////////////////////////////////////////////////////////////
      vector< vector<Point> > strokes = ssToOrderedPoints(iss);
      allStrokes.push_back(strokes);
    }
  }
  //Write to File
  ofstream file;
  file.open("out.pt");
  //count strokes
  int totalStrokes = 0;
  for (vector< vector< vector<Point> > >::iterator it = allStrokes.begin(); it != allStrokes.end(); it++) {
    vector< vector<Point> > strokes = *it;
    totalStrokes += strokes.size();
  }
  file << "PT 2 " << totalStrokes << endl; //Stroke Header
  file << "#Generated by skel.cc" << endl;
  for (vector< vector< vector<Point> > >::iterator it1 = allStrokes.begin(); it1 != allStrokes.end(); it1++) {
    vector< vector<Point> > strokes = *it1;
    int strokeNum = 0;
    for (vector< vector<Point> >::iterator it2 = strokes.begin(); it2 != strokes.end(); it2++) {
      vector<Point> points = *it2;
      int numPoints = points.size();
      file << "0 " << numPoints << endl; //Point Header
      for (vector<Point>::iterator it3 = points.begin(); it3 != points.end(); it3++) {
        Point p = *it3;
        file << " " << p.x << " " << -p.y << endl;
      }
    }
  }
  file.close();


  imshow("Contours", imgContours);
  moveWindow("Contours", 300, 250);
  waitKey();
  return 0;
}




